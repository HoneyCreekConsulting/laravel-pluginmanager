<?php

namespace HoneyCreekConsulting\PluginManager;

class PluginManagerResponse {
    public bool $success;
    public string $message;
    public int $errorCode;
    public object $data;
}

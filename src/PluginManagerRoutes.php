<?php
namespace HoneyCreekConsulting\PluginManager;


use Illuminate\Support\Facades\Route;

Route::get('/api/v1/plugins', [PluginManagerController::class, 'getPlugins']);
Route::get('/api/v1/plugins/{id}', [PluginManagerController::class, 'GetPluginById']);

Route::delete('/api/v1/plugins', function () {
    return "Deleted plugins";
});
Route::delete('/api/v1/plugins/{id}', function ($id) {
    return "Deleted plugin: " . $id;
});

Route::post('/api/v1/plugins', function () {
    return "Created a new plugin";
});

Route::put('/api/v1/plugins/{plugin}', function (Plugin $plugin) {
    return $plugin;
});

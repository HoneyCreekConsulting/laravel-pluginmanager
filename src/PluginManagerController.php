<?php

namespace HoneyCreekConsulting\PluginManager;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

class PluginManagerController extends Controller
{
    protected PluginManager $pluginManager;

    public function __construct(PluginManager $pluginManager)
    {
        $this->pluginManager = $pluginManager;
    }


    /**
     * Get all Plugins that are currently installed in the plugins directory
     *
     * @return array
     */
    public function GetPlugins(): array
    {
        $getAllPluginResponse = new PluginManagerResponse();
        $plugins = $this->pluginManager->getPlugins();
        if (sizeof($plugins) == 0) {
            $getAllPluginResponse->message = "Unable to find any plugins";
            $getAllPluginResponse->success = false;
            $getAllPluginResponse->errorCode = 1000;
        }
        foreach ($plugins as $plugin) {
            $isDirectoriesDisabled = Config::get('plugin-manager')['DisableDirectories'];
            if (is_bool($isDirectoriesDisabled)) {
                if ($isDirectoriesDisabled) {
                    unset($plugin->{'installationPath'});
                }
            }
        }
        $data = [
            "plugins" => $plugins
        ];
        $getAllPluginResponse->success = true;
        $getAllPluginResponse->message = "Plugins successfully accessed";
        $getAllPluginResponse->data = (object)$data;
        return (array)$getAllPluginResponse;
    }

    public function GetPluginById(int $id): string
    {
        $getPluginResponse = new PluginManagerResponse();
        $plugins = $this->pluginManager->getPlugins();
        if (sizeof($plugins) == 0) {
            $getPluginResponse->message = "Unable to find any plugins";
            $getPluginResponse->success = false;
            $getPluginResponse->errorCode = 1000;
        }

        $data = [
            "plugins" => $plugins
        ];
        $getPluginResponse->success = true;
        $getPluginResponse->message = "Plugins successfully accessed";
        $getPluginResponse->data = (object)$data;
        return (array)$getPluginResponse;
    }

    public function DeletePluginById(string $id): mixed
    {
        $plugins = $this->pluginManager->getPlugins();
        foreach ($plugins as $plugin) {
            if ($plugin->id == $id) {
                return new PluginResponse('success', 200, ["Plugin found"], $plugin);

            }
        }
        return $pluginFound;
    }

    /*
     * Create Plugin is used to install the plugin into the location it should be installed into.
     *
     */
    public function CreatePlugin(): void
    {
        $plugins = $this->pluginManager->getPlugins();
        //return new PluginResponse("Unable to send response");

        //return new PluginResponse("Plugin not found with id: $id", 404, [], new stdClass());
    }
}


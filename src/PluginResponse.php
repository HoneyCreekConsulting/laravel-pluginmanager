<?php
namespace HoneyCreekConsulting\PluginManager;


use JetBrains\PhpStorm\Pure;

class PluginResponse
{
    public string $status;
    public int $code;
    public array $messages;
    public object $result;


    #[Pure] public function __construct(string $status, int $code, array $messages, object $result)
    {

            $this->status = $status;
            $this->code = $code;

            $this->messages = $messages;
            $this->result = $result;

    }
    public function __toString(): string
    {
        return json_encode($this) ?: '';

    }
}
